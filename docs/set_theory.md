# Set Theory

## Size

A set can be **finite** or **infinite**, we can find the number of elements in it

$$
\begin{aligned}
    & |A| = k \;|\; \infty
\end{aligned}
$$

---

## Subset or Equal

$$
\begin{aligned}
    & A \subseteq B
    \\
    & \forall a \in A : a \in B
\end{aligned}
$$

## Subset and Not Equal

$$
\begin{aligned}
    & A \subsetneq B
    \\
    & \forall a \in A : a \in B, \exists a_k \in B : a_k \notin A
\end{aligned}
$$

## Union

$$
\begin{aligned}
    & A \cup B
\end{aligned}
$$

## Intersect

$$
\begin{aligned}
    & A \cap B
\end{aligned}
$$

## Difference

$$
\begin{aligned}
    & A - B
\end{aligned}
$$

## Complement

$$
\begin{aligned}
    & \overline{A} = A' = U - A
\end{aligned}
$$

-   If $A$ is **finite** then $\overline{A}$ is **infinite**
-   If $A$ is **infinite** we cannot say anything about $\overline{A}$
