# Recurrence

The recurrences are exists to simplify the harder problems solutions by break them into smaller repetitieve solutions

## Recurrence Relation (Difference Equations)

Is an **equation** that recursively defines a **sequence** where the next term ($\textbf{a}_n$) is a function of the previous terms ($\textbf{a}_{n-1}, \textbf{a}_{n-2}, \dots$)

$$
\begin{aligned}
    & F_{n} = F_{n-1} + F_{n-2}
    \\
    & \texttt{Degree (Number of dependent terms)}: 2
\end{aligned}
$$

> **Tip**
>
> Recurrence relation terms are **Function Calls** so:
>
> $$
> \begin{aligned}
>     & F_n = F(n)
> \end{aligned}
> $$

### Recurrence Sequence

Is a **Recurrence Relation** with **Initial Conditions** to break the infinite loop

$$
\begin{aligned}
    & \texttt{Fibonacci series}:
    \\
    \\
    & F_{n} = F_{n-1} + F_{n-2}
    \\
    & \texttt{I.C}:
    \begin{cases}
        & F_{0} = 0
        \\
        & F_{1} = 1
    \end{cases}
\end{aligned}
$$

> **Tip**
>
> We need **Initial Conditions** equals to **degree** of recurrence relation

---

## Recurrence Relation Types

There are many types of **Recurrence Relations**

1. **Linear**: Linear combination of **recurrence** terms
    1. **Homogeneous**: Without **non-recurrence** term
    2. **Non Homogeneous**: With **non-recurrence** term
2. **Non Linear**: Non-Linear combination of **recurrence** terms
    1. **Devide & Conquer**

---

### Linear

Is a **Recurrence Relation** where next term is only depends on **Linear Combination** of **Sequence of previous terms** ($F_{n-1}, F_{n-2}, \dots$ not $F_{\frac{n}{2}}, F_{\frac{n}{4}}, F_{n}^2, \sqrt{F_n}, n.F_n, \dots$):

$$
\begin{aligned}
    & F_{n} = c_1.F_{n-1} + c_2.F_{n-2} + \dots + c_k.F_{n-k}
\end{aligned}
$$

---

#### Homogeneous

Is a **Linear Recurrence Relation** that does not contains any non-recurrence terms (**depends on `n`**):

$$
\begin{aligned}
    & F_{n} = c_1.F_{n-1} + c_2.F_{n-2} + \dots + c_k.F_{n-k}
\end{aligned}
$$

---

#### Non-Homogeneous

Is a **Linear Recurrence Relation** that contains **One or Many** non-recurrence terms (**depends on `n`**):

$$
\begin{aligned}
    & F_{n} = c_1.F_{n-1} + c_2.F_{n-2} + \dots + c_k.F_{n-k} + \textbf{f(n)}
\end{aligned}
$$

---

### Non Linear

Is a **Recurrence Relation** where next term is depends on any **Non-Linear Combination** of **Previous terms** ($F_{\frac{n}{2}}, F_{\frac{n}{4}}, F_{n}^2, \sqrt{F_n}, n.F_n, \dots$):

$$
\begin{aligned}
    & F_{n} = 2.F_{\frac{n}{2}} + n^2
    \\
    \\
    & F_{n} = F_{\frac{n}{3}} + F_{\frac{2n}{3}} + n
    \\
    \\
    & F_{n} = 2.F_{\sqrt{n}} + \log{n}
    \\
    \\
    & F_{n} = F_{n-1} . F_{n-2}^{2}
    \\
    \\
    & F_{n} = \sqrt{\frac{1}{2}.F_{n-1}^2 + \frac{1}{2}.F_{n-2}^2 + n}
    \\
    \\
    & F_{n} = 3n.F_{n-1} - 2n.(n-1).F_{n-2}
    \\
    \\
    & F_{n} = 4.F_{\frac{n}{2}} - 4.F_{\frac{n}{4}} + n.\log{n}^{2}
\end{aligned}
$$

---

## Solving Methods

Solving means converting a **Recurrence Relation** to a **Direct Relation**

There are many ways to solving

---

### Bottom To Top Method

Start from $F_1$ to $F_n$ and visually try to find a direct relation of items

In this way we try to write some terms like $F_1, F_2, \dots, F_k$ then we try to find the relation between terms

---

-   **Example 1**:

$$
\begin{aligned}
    & a_n = a_{n-1} + n
    \\
    & a_0 = 0
    \\
    \\ \implies
    & a_0 = 0
    \\
    & a_1 = 0 + 1
    \\
    & a_2 = 0 + 1 + 2
    \\
    & a_3 = 0 + 1 + 2 + 3
    \\
    & \dots
    \\
    & a_n = 0 + 1 + 2 + \dots + n = \sum_{i=0}{n} i = \frac{n.(n-1)}{2}
\end{aligned}
$$

---

### Top To Bottom Method

Start from $F_n$ to $F_1$ and visually try to find a direct relation of items

In this way we try to break $F_n$ into previous terms and do it to find $F_n$ based on summation of $F_1$

---

-   **Example 1**:

$$
\begin{aligned}
    & T(n) = T(n-1) + \log{n}
    \\
    & T(1) = 0
    \\
    \\ \implies
    & T(n) = \log{(n)} + T(n-1)
    \\
    & T(n) = \log{(n)} + \log{(n-1)} + T(n-2)
    \\
    & T(n) = \log{(n)} + \log{(n-1)} + \log{(n-2)} + T(n-3)
    \\
    & \dots
    \\
    & T(n) = \log{(n)} + \log{(n-1)} + \log{(n-2)} + \dots + \log{(1)} =
    \\
    \\ \implies
    & \sum_{i=0}^{n} \log{i} = \log{(\prod_{i=0}^{n} i)} = \log{(n!)}
\end{aligned}
$$

---

### Recurrence Tree Method

Is a visualized version of **Top To Bottom Method** using a **Tree Structure**

In this method we will find a boundry for our **Recurrence Relation**:

1. Next term in **root**
2. Previous terms with **coefficient 1** in **leafs**
3. Grow the tree until you get the **initial conditions**
4. Summerize the **rows** of tree
5. Find the **longest path** and **shortest path** of tree
6. Grow is among $\Omega(MinPath . Row)$ and $O(MaxPath . Row)$

> **Tip**
>
> If the **Min Path** and **Max Path** are equal, tree is **Balanced** so we can find the exact **Summation** value, but else tree is **Unbalanced** and we can find a **Boundry** for it

---

-   **Example 1**:

$$
\begin{aligned}
    & T(n) = T(\frac{n}{10}) + T(\frac{9n}{10}) + n
    \\
    & T(1) = 1
    \\
    \\
    & MinPath: 1 = \frac{n}{10^k} \implies k = \log_{10}{n}
    \\
    & MaxPath: 1 = \frac{9^k n}{10^k} \implies k = \log_{\frac{10}{9}}{n}
    \\
    & Sum(i): n
    \\
    \\ \implies
    & \sum_{i=1}^{\log_{10}{n}}(n) \leq T(n) \leq \sum_{i=1}^{\log_{\frac{10}{9}}{n}}(n)
    \\
    \\ \implies
    & n.\log_{10}{n} \leq T(n) \leq n.\log_{\frac{10}{9}}{n}
    \\
    \\ \implies
    & T(n) \in \Theta(n \log{n})
\end{aligned}
$$

![Recurrence Tree](./assets/recurrence_tree.png)

---

-   **Example 2**:

$$
\begin{aligned}
    & T(n) = T(\frac{n}{3}) + T(\frac{2n}{3}) + n^2
    \\
    & T(1) = 1
    \\
    \\
    & MinPath: 1 = \frac{n}{3^k} \implies k = \log_{3}{n}
    \\
    & MaxPath: 1 = \frac{2^k n}{3^k} \implies k = \log_{\frac{3}{2}}{n}
    \\
    & Sum(i): (\frac{5}{9})^{i-1}.n^2
    \\
    \\ \implies
    & \sum_{i=1}^{\log_{10}{n}}((\frac{5}{9})^{i-1}.n^2) \leq T(n) \leq \sum_{i=1}^{\log_{\frac{10}{9}}{n}}((\frac{5}{9})^{i-1}.n^2)
    \\
    \\ \implies
    & T(n) \in \Theta(n^2 \log{n})
\end{aligned}
$$

---

-   **Example 3**:

$$
\begin{aligned}
    & T(n) = 2T(\sqrt{n}) + \log{n} \implies T(\sqrt{n}) + T(\sqrt{n}) + \log{n}
    \\
    & T(1) = 1
    \\
    \\
    & MinPath: 1 = \log{(n^{\frac{1}{2^k}})} \implies 2^k = \log{n} \implies k = \log\log{n}
    \\
    & MaxPath: 1 = \log{(n^{\frac{1}{2^k}})} \implies 2^k = \log{n} \implies k = \log\log{n}
    \\
    & Sum(i): \log{n}
    \\
    \\ \implies
    & T(n) = \sum_{i=1}^{\log\log{n}}(\log{n})
    \\
    \\ \implies
    & T(n) \in \Theta(\log{n}.\log\log{n})
\end{aligned}
$$

---

-   **Example 4**: We can use **Recurrence Tree Method** for **Multi-Variable Recurrence Relations**:

-   The tip of this type of relations is to find the **Longest Path** based on previous terms

$$
\begin{aligned}
    & T(n,k) = T(\frac{n}{2}, k) + T(n, \frac{k}{4}) + nk
    \\
    & T(1,*) = T(*,1) = 1
    \\
    \\
    & MinPath: \log_{4}{n}
    \\
    & MaxPath: \log_{2}{n} + \log_{4}{n}
    \\
    & Sum(i): nk.(\frac{3}{4})^{i-1}
    \\
    \\ \implies
    & nk.\sum_{i=1}^{\log_{4}{n}}(\frac{3}{4})^{i-1} \leq T(n,k) \leq nk.\sum_{i=1}^{\log_{2}{n} + \log_{4}{n}}(\frac{3}{4})^{i-1}
    \\
    \\ \implies
    & T(n) \in \Theta(nk.\log{n})
\end{aligned}
$$

![Multi Variable Recurrence](./assets/multi_variable_recurrence.png)

---

### Change Of Variables Method

This method is used for **Non-Linear Recurrence Relations**, using this method, we convert a **Non-Linear Recurrence** to a **Linear Recurrence** then we solve it using before methods

1. **Change style of equation sides to be similar**
    1. **Multiply**: $x.y.z \overset{\log{?}}{\implies} \log{x}+\log{y}+\log{z}$
    2. **Power**: $x^3 \overset{\log{?}}{\implies} 3\log{x}$
    3. **Division**: $\frac{x}{y} \overset{\log{?}}{\implies} \log{x}-\log{y}$
    4. **Sqrt**: $\sqrt{x} \overset{\log{?}}{\implies} \frac{1}{2}\log{x}$
    5. **Logarithm**: $\log{x} \overset{e^{?}}{\implies} = x$
    6. **Factorial**
2. **Change of variables to linearize**
    1. **Inside**: $n = 2^m, \dots$
    2. **Outside**: $b(n) = T(2^n)$, $b(n) = \frac{T(n)}{n}, \dots$

---

-   **Example 1**:

$$
\begin{aligned}
    & a_n = a_{n-1} . a_{n-2}^2
    \\
    & a_0 = 1
    \\
    & a_1 = 2
    \\
    \\ \implies
    & \log{a_n} = \log{(a_{n-1} . a_{n-2}^2)} = \log{a_{n-1}} + 2.\log{a_{n-2}}
    \\
    \\ \implies
    & b(n) = \log{a_n}
    \\
    \\ \implies
    & b(n) = b(n-1) + 2.b(n-2)
\end{aligned}
$$

---

-   **Example 2**:

$$
\begin{aligned}
    & T(n) = \sqrt{\frac{1}{2} T^2(n-1) + \frac{1}{2} T^2(n-2) + n}
    \\
    \\ \implies
    & \log{T(n)} = \log{(\sqrt{\frac{1}{2} T^2(n-1) + \frac{1}{2} T^2(n-2) + n})}
    \\
    \\ \implies
    & \log{T(n)} = \frac{1}{2}.\log{(\frac{1}{2} T^2(n-1) + \frac{1}{2} T^2(n-2) + n)}
    \\
    \\ \implies
    & 2.\log{T(n)} = \log{(\frac{1}{2} T^2(n-1) + \frac{1}{2} T^2(n-2) + n)}
    \\
    \\ \implies
    & e^{2.\log{T(n)}} = e^{\log{(\frac{1}{2} T^2(n-1) + \frac{1}{2} T^2(n-2) + n)}}
    \\
    \\ \implies
    & e^{\log{T(n)^2}} = (\frac{1}{2} T^2(n-1) + \frac{1}{2} T^2(n-2) + n)
    \\
    \\ \implies
    & T(n)^2 = \frac{1}{2} T^2(n-1) + \frac{1}{2} T^2(n-2) + n
    \\
    \\ \implies
    & b(n) = T(n)^2
    \\
    \\ \implies
    & b(n) = \frac{1}{2} b(n-1) + \frac{1}{2} b(n-2) + n
\end{aligned}
$$

---

-   **Example 3**:

$$
\begin{aligned}
    & a_n = 3na_{n-1} - 2n(n-1)a_{n-2}
    \\
    \\ \implies
    & b(n) = \frac{a_n}{n!}
    \\
    \\ \implies
    & b(n) = 3b(n-1) - 2b(n-2)
\end{aligned}
$$

---

-   **Example 4**:

$$
\begin{aligned}
    & T(n) = 4T(\frac{n}{2}) - 4T(\frac{n}{4}) + n\log^2{n}
    \\
    \\ \implies
    & n = 2^m
    \\
    \\ \implies
    & T(2^m) = 4T(2^{m-1}) - 4T(2^{m-2}) + 2^m.\log^2{2^m}
    \\
    \\ \implies
    & b(m) = T(2^m)
    \\
    \\ \implies
    & b(m) = 4b(m-1) - 4b(m-2) + 2^m.m^2.\log^2{2}
    \\
    \\ \implies
    & b(m) = 4b(m-1) - 4b(m-2) + 2^m.m^2
\end{aligned}
$$

---

### Boundary Finding method

Sometimes by finding the boundry of a **Recursive Function** we can find that direct formula more simpler

---

-   **Example 1**:

$$
\begin{aligned}
    & T(n) = 2T(\frac{n}{\log{n}}) + n
    \\ \implies
    & n \ll 2T(\frac{n}{\log{n}}) + n \ll 2T(\frac{n}{4}) + n
    \\ \implies
    & \Theta(n) \ll 2T(\frac{n}{\log{n}}) + n \ll \Theta(n)
    \\ \implies
    & T(n) \in \Theta(n)
\end{aligned}
$$

---

-   **Example 2**:

$$
\begin{aligned}
    & T(n) = T(\frac{n}{3}) + T(\frac{2n}{3}) + n^2
    \\ \implies
    & n^2 \ll T(\frac{n}{3}) + T(\frac{2n}{3}) + n^2 \ll T(\frac{2n}{3}) + T(\frac{2n}{3}) + n^2
    \\ \implies
    & \Theta(n^2) \ll T(\frac{n}{3}) + T(\frac{2n}{3}) + n^2 \ll \Theta(n^2)
    \\ \implies
    & T(n) \in \Theta(n^2)
\end{aligned}
$$

---

-   **Example 3**:

$$
\begin{aligned}
    & T(n) = T(n-1) + 2T(n-2) + T(n-3)
    \\ \implies
    & T(n) \lt 4T(n-1)
    \\ \implies
    & T(n) \in \Theta(4^n)
\end{aligned}
$$

---

### Characteristic Equation Method

This direct method used for solving **Linear Recurrence Relations**:

1. **Homogeneous**
2. **Non-Homogeneous**

The algorithm for both is the same with some differences:

1. **Write Characteristic Equation**: **Different**
2. **Find roots**: $x_1, x_2, \dots, x_k$
3. **Write Unique Roots Terms**: $\alpha_1.x_1^n, \alpha_2.x_2^n, \dots, \alpha_k.x_k^n$
4. **Write Repeated Roots Terms**: $(\alpha_i + \alpha_j.n + \alpha_k.n^2).x_i^n, \dots$
5. **Write Direct Relation**: Sum of **root terms**
6. **Find alphas**: $\alpha_1, \alpha_2, \dots, \alpha_k$

> **Tip**
>
> **Root Terms** are linear combination of $\alpha$ and $n$ for same $x_i, x_j, \dots$
>
> $$
> \begin{aligned}
>     & x_i \implies (\alpha_i).x_i^n
>     \\
>     & x_i = x_j \implies (\alpha_i + \alpha_j.n).x_i^n
>     \\
>     & x_i = x_j = x_k \implies (\alpha_i + \alpha_j.n + \alpha_k.n^2).x_i^n
>     \\
>     & \dots
> \end{aligned}
> $$

$$
\begin{aligned}
    & \texttt{Recurrence Relation}:
    \\ \implies
    & a_n = c_1.a_{n-1} + c_2.a_{n-2} + \dots + c_k.a_{n-k} + f(n)
    \\
    \\
    & \texttt{Characteristic Equation}:
    \\ \implies
    & ???
    \\
    \\
    & \texttt{Roots}:
    \\ \implies
    & x_1, x_2, \dots, x_k
    \\
    \\
    & \texttt{Root Terms}:
    \\ \implies
    & x_1 \implies (\alpha_1).x_1^n
    \\
    & x_2 \implies (\alpha_2).x_2^n
    \\
    & x_i = x_j = x_k \implies (\alpha_i + \alpha_j.n + \alpha_k.n^2).x_i^n
    \\
    & \dots
    \\
    \\
    & \texttt{Direct Relation}:
    \\ \implies
    & a_n = \alpha_1.x_1^n + \alpha_2.x_2^n + \dots +
    (\alpha_f + \alpha_p.n).x_f^n + (\alpha_i + \alpha_j.n + \alpha_k.n^2).x_i^n
\end{aligned}
$$

---

#### Homogeneous

The homogeneous **Characteristic Equation** is:

$$
\begin{aligned}
    & \texttt{Homogeneous Recurrence Relation}:
    \\ \implies
    & a_n = c_1.a_{n-1} + c_2.a_{n-2} + \dots + c_k.a_{n-k}
    \\
    \\
    & \texttt{Characteristic Equation}:
    \\ \implies
    & x^k = c_1.x^{k-1} + c_2.x^{k-2} + \dots + c_k.x_{0}
\end{aligned}
$$

---

-   **Example 1**:

$$
\begin{aligned}
    & F_n = F_{n-1} + F_{n-2}
    \\
    & F_0 = 0
    \\
    & F_1 = 1
    \\
    & Degree = k = 2
    \\
    \\ \implies
    & x^2 = x^1 + x^0 \implies x^2 - x - 1 = 0
    \\
    \\ \implies
    & x_1 = \frac{1 + \sqrt{5}}{2} \implies \alpha_1.(\frac{1 + \sqrt{5}}{2})^n
    \\
    & x_2 = \frac{1 - \sqrt{5}}{2} \implies \alpha_2.(\frac{1 - \sqrt{5}}{2})^n
    \\
    \\ \implies
    & F_n = \alpha_1.(\frac{1 + \sqrt{5}}{2})^n + \alpha_2.(\frac{1 - \sqrt{5}}{2})^n
    \\
    \\
    & F_0 = 0 \implies 0 = \alpha_1 + \alpha_2
    \\
    & F_1 = 1 \implies 1 = \alpha_1.(\frac{1 + \sqrt{5}}{2}) + \alpha_2.(\frac{1 - \sqrt{5}}{2})
    \\
    \\ \implies
    & \alpha_1 = \frac{1}{\sqrt{5}}
    \\
    & \alpha_2 = -\frac{1}{\sqrt{5}}
    \\
    \\ \implies
    & F_n = \frac{1}{\sqrt{5}}.(\frac{1 + \sqrt{5}}{2})^n - \frac{1}{\sqrt{5}}.(\frac{1 - \sqrt{5}}{2})^n
\end{aligned}
$$

---

-   **Example 2**:

$$
\begin{aligned}
    & a_n = 4.a_{n-1} - 4.a_{n-2}
    \\
    & a_0 = 1
    \\
    & a_1 = 2
    \\
    & Degree = k = 2
    \\
    \\ \implies
    & x^2 = 4.x^1 - 4.x^0 \implies x^2 - 4.x + 4 = 0
    \\
    \\ \implies
    & x_1 = x_2 = 2 \implies (\alpha_1 + \alpha_2.n).(2)^n
    \\
    \\ \implies
    & a_n = (\alpha_1 + \alpha_2.n).(2)^n
    \\
    \\
    & a_0 = 1 \implies 1 = (\alpha_1 + \alpha_2.0).2^0
    \\
    & a_1 = 2 \implies 2 = (\alpha_1 + \alpha_2.1).2^1
    \\
    \\ \implies
    & \alpha_1 = 1
    \\
    & \alpha_2 = 0
    \\
    \\ \implies
    & a_n = 2^n
\end{aligned}
$$

---

#### Non-Homogeneous

The non-homogeneous **Characteristic Equation** is:

$$
\begin{aligned}
    & \texttt{Non-Homogeneous Recurrence Relation}:
    \\ \implies
    & a_n = c_1.a_{n-1} + c_2.a_{n-2} + \dots + c_k.a_{n-k} + f(n)
    \\
    \\
    & f(n) = b^n . p(n)
    \\
    & b: \texttt{Constant}
    \\
    & p(n): \texttt{Linear combination degree of d}
    \\
    \\
    & \texttt{Characteristic Equation}:
    \\ \implies
    & (x^k - c_1.x^{k-1} - c_2.x^{k-2} - \dots - c_k.x_{0}).(x - b)^{d+1} = 0
\end{aligned}
$$

-   **Example 1**:

$$
\begin{aligned}
    & a_{n+1} = 4.a_{n-1} + 2^{n+4} + n.2^n + 5n + 6
    \\ \implies
    & (a_{n+1} - 4.a_{n-1}) = 2^n . (2^4 + n) + 1^n.(6 + 5n)
    \\
    & Degree = k = 2
    \\
    \\ \implies
    & (x^2 - 4).(x - 2)^{2}.(x - 1)^{2} = 0
    \\
    \\ \implies
    & x_1 = x_3 = x_4 = 2 \implies (\alpha_1 + \alpha_3.n + \alpha_4.n^2).2^n
    \\
    & x_2 = -2 = \implies (\alpha_2).(-2)^n
    \\
    & x_5 = x_6 = 1 \implies (\alpha_5 + \alpha_6.n).1^n
    \\
    \\ \implies
    & a_n = (\alpha_1 + \alpha_3.n + \alpha_4.n^2).2^n + (\alpha_2).(-2)^n + (\alpha_5 + \alpha_6.n).1^n
    \\
    \\ \implies
    & \dots
\end{aligned}
$$

---

### Master Method

This method is used to find **Growth Of Function** for a specific category of **Non-Linear Recurrences**

We compare $f(n)$ and $n^{\log_{b}^{a}}$ to find the complexity (Biggest)

We can express the Master method in two ways:

-   **Using Growth Of Function**:

$$
\begin{aligned}
    & T(n) = a T(\frac{n}{b}) + f(n) \implies
    \begin{cases}
        f(n) \ll n^{\log_{b}^{a}} &\implies T(n) \in \Theta(n^{\log_{b}^{a}})
        \\
        \\
        f(n) \gg n^{\log_{b}^{a}} &\implies T(n) \in \Theta(f(n))
        \\
        \\
        \begin{cases}
            f(n) \sim n^{\log_{b}^{a}}
            \\
            \frac{f(n)}{n^{\log_{b}^{a}}} = \log^{k}{n}
        \end{cases}
        &\implies T(n) \in \Theta(f(n).\log{n})
    \end{cases}
    \\
    & a,b: \texttt{Constant}
    \\
    & a \geq 1, b \geq 1
\end{aligned}
$$

-   **Using Asymptotic Notations**:

$$
\begin{aligned}
    & T(n) = a T(\frac{n}{b}) + f(n) \implies
    \begin{cases}
        f(n) \in O(n^{\log_{b}^{a} - \epsilon}) \implies& T(n) \in \Theta(n^{\log_{b}^{a}})
        \\
        \\
        f(n) \in \Omega(n^{\log_{b}^{a} + \epsilon}) \implies& T(n) \in \Theta(f(n))
        \\
        \\
        \begin{cases}
            f(n) \in \Theta(n^{\log_{b}^{a}})
            \\
            \frac{f(n)}{n^{\log_{b}^{a}}} = \log^{n}{k}
        \end{cases}
        \implies& T(n) \in \Theta(f(n).\log{n})
    \end{cases}
    \\
    & a,b: \texttt{Constant}
    \\
    & a \geq 1, b \geq 1
\end{aligned}
$$

> **Tip**
>
> $$
> \begin{aligned}
>     & n^{\epsilon} \gg \log^{k}{n}
> \end{aligned}
> $$

---

-   **Example 1**:

$$
\begin{aligned}
    & T(n) = 2T(\frac{n}{2}) + n^2
    \\
    & n^{\log_{2}^{2}} \;?\; n^2 \implies n \ll n^2 \implies T(n) \in \Theta(n^2)
    \\
    \\
    & T(n) = 2T(\frac{n}{2}) + n^2.\log{n}
    \\
    & n^{\log_{2}^{2}} \;?\; n^2.\log{n} \implies n \ll n^2.\log{n} \implies T(n) \in \Theta(n^2.\log{n})
    \\
    \\
    & T(n) = 3T(\frac{2n}{3}) + n^3
    \\
    & n^{\log_{\frac{3}{2}}^{3}} \;?\; n^3 \implies n^{2.\dots} \ll n^3 \implies T(n) \in \Theta(n^3)
\end{aligned}
$$

---

-   **Example 2**:

$$
\begin{aligned}
    & T(n) = 4T(\frac{n}{2}) + 2n + 1
    \\
    & n^{\log_{2}^{4}} \;?\; 2n + 1 \implies n^2 \gg 2n + 1 \implies T(n) \in \Theta(n^2)
    \\
    \\
    & T(n) = 4T(\frac{n}{2}) + n.\log{n}
    \\
    & n^{\log_{2}^{4}} \;?\; n.\log{n} \implies n^2 \gg n.\log{n} \implies T(n) \in \Theta(n^2)
\end{aligned}
$$

---

-   **Example 3**:

$$
\begin{aligned}
    & T(n) = 2T(\frac{n}{2}) + n - 1
    \\
    & n^{\log_{2}^{2}} \;?\; n - 1 \implies n \sim n - 1 \implies T(n) \in \Theta(n.\log{n})
    \\
    \\
    & T(n) = 2T(\frac{n}{2}) + n.\log{n}
    \\
    & n^{\log_{2}^{2}} \;?\; n.\log{n} \implies \frac{n.\log{n}}{n} = \log{n} \implies T(n) \in \Theta(n.(\log{n})^2)
    \\
    \\
    & T(n) = 4T(\frac{n}{2}) + (\log{(n!)})^2
    \\
    & n^{\log_{2}^{4}} \;?\; (n.\log{n})^2 \implies \frac{n^2.(\log{n})^2}{n^2} = (\log{n})^2 \implies T(n) \in \Theta(n^2.(\log{n})^3)
    \\
    \\
    & T(n) = 2T(\frac{n}{2}) + n.\log\log{n}
    \\
    & n^{\log_{2}^{2}} \;?\; n.\log\log{n} \implies \frac{n.\log\log{n}}{n} = \log\log{n} \implies T(n) \in \Theta(n.\log{n}.\log\log{n
    })
\end{aligned}
$$

---

### Akra–Bazzi Method

Is a special method for **Special Non-Linear Recurrence Relations**

$$
\begin{aligned}
    & T(n) = a_1 T(\frac{n}{b_1}) + a_2 T(\frac{n}{b_2}) + \dots + a_k T(\frac{n}{b_k}) + f(n)
    \\
    & a_1, a_2, \dots, a_k \gt 0
    \\
    & b_1, b_2, \dots, b_k \gt 0
    \\
    & p: \texttt{Root of equation}:\quad \frac{a_1}{b_1^p} + \frac{a_2}{b_2^p} + \dots + \frac{a_k}{b_k^p} = 1
    \\
    \\ \implies
    & T(n) \in \Theta(n^p (1 + \int_{1}^{n} \frac{f(x)}{x^{p+1}} dx))
\end{aligned}
$$

-   There are exists a special version of **Akra Bazzi** where **f(n)** is a **Linear Function**:

$$
\begin{aligned}
    & T(n) = a_1 T(\frac{n}{b_1}) + a_2 T(\frac{n}{b_2}) + \dots + a_k T(\frac{n}{b_k}) + f(n)
    \\
    \\ \implies
    & \frac{a_1}{b_1} + \frac{a_2}{b_2} + \dots + \frac{a_k}{b_k} = 1 \implies T(n) \in \Theta(n.\log{n})
    \\ \implies
    & \frac{a_1}{b_1} + \frac{a_2}{b_2} + \dots + \frac{a_k}{b_k} \lt 1 \implies T(n) \in \Theta(n)
    \\
    \\
    & T(n) = a_1 T(\frac{n}{b_1}) + a_2 T(\frac{n}{b_2}) + \dots + a_k T(\frac{n}{b_k}) + f(n^p)
    \\
    \\ \implies
    & \frac{a_1}{b_1} + \frac{a_2}{b_2} + \dots + \frac{a_k}{b_k} \leq 1 \implies T(n) \in \Theta(n^p)
\end{aligned}
$$

-   There are exists a special version of **Akra Bazzi** where **f(n)** is a **Linear Function** for **Two Terms**:

$$
\begin{aligned}
    & T(n) = T(\alpha n) + T(\beta n) + f(n)
    \\
    \\ \implies
    & \alpha + \beta = 1 \implies T(n) \in \Theta(n.\log{n})
    \\ \implies
    & \alpha + \beta \lt 1 \implies T(n) \in \Theta(n)
    \\
    \\
    & T(n) = T(\alpha n) + T(\beta n) + f(n^p)
    \\
    \\ \implies
    & \alpha + \beta \leq 1 \implies T(n) \in \Theta(n^p)
\end{aligned}
$$

---

-   **Example 1**:

$$
\begin{aligned}
    & T(n) = T(\frac{n}{3}) + T(\frac{2n}{3}) + n
    \\
    & a_1 = 1,\; a_2 = 1
    \\
    & b_1 = 3,\; b_2 = \frac{3}{2}
    \\
    & f(n) = n
    \\
    & \frac{1}{3^p} + \frac{1}{(\frac{3}{2})^p} = 1 \implies p = 1
    \\
    \\ \implies
    & T(n) \in \Theta(n^1 (1 + \int_{1}^{n} \frac{x}{x^2} dx))
    \\ \implies
    & T(n) \in \Theta(n (1 + \log{n}))
    \\ \implies
    & T(n) \in \Theta(n.\log{n})
\end{aligned}
$$

---
